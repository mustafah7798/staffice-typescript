import React, { useCallback, useEffect, useState } from 'react';

import { Image } from '@chakra-ui/image';
import { Flex } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { useNavigate } from 'react-router-dom';
import useWebSocket, { ReadyState } from 'react-use-websocket';

import { useReduxDispatch, useReduxSelector } from '@/config/redux/root-store';
import { useGetAssetsQuery } from '@/service/api/asset/asset-api';
import {
  useDeleteDeviceMutation,
  useEditDeviceMutation,
  useGetAllDevicesQuery,
  useGetDeviceQuery,
  useGetDeviceTelemetriesQuery,
  useGetDeviceTokenQuery,
  useSendRpcCommandMutation,
} from '@/service/api/device/device-api';
import { setDeviceName } from '@/stores/device-store';

import MenuButton from '../menu-button/MenuButton';
import { EditUserProfileScrollMenu } from './EditUserProfileScrollMenu';

interface ScrollMenuProps {
  selected?: SelectedMenu;
  onChangeSelection?: (selected: SelectedMenu) => void;
}

export enum SelectedMenu {
  List = 0,
  Edit = 1,
  Room = 2,
  Home = 3,
  RoomEdit = 4,
  AssetSettings = 5,
  DeviceSettings = 6,
}

export const HomeScrollMenu: React.FC<ScrollMenuProps> = (props) => {
  const navigate = useNavigate();
  const routeChange = () => {
    const path = `/`;
    navigate(path);
  };

  const [selected, setSelected] = useState<SelectedMenu>(SelectedMenu.List);

  useEffect(() => {
    if (props.selected !== undefined) {
      setSelected(props.selected);
    }
  }, [props.selected]);

  useEffect(() => {
    props.onChangeSelection?.(selected);
  }, [selected]);
  const deviceId = useReduxSelector((store) => store.device.DeviceId);
  const assetId = useReduxSelector((store) => store.asset.assetId);
  const roomName = useReduxSelector((store) => store.room.roomName);
  const [sendRpcCommand] = useSendRpcCommandMutation();

  const editDeviceName = async () => {
    console.log('edit device name');

    // return await editDevice({
    //   assetId: 'a2a5a230-ce8c-11ec-b13a-e383b3bf7859',
    //   deviceId: '0b867b40-f470-11ec-ab40-d386cc720766',
    //   deviceIdentifier: 'C6:4F:33:D5:8B:00',
    //   label: 'label',
    //   type: 'parlar.three_pole_switch_with_wifi.v1',
    //   zone: 'کتابخانه  5',
    //   deviceProfileId: 'e00de200-8255-11ec-b5c6-494c371367cd',
    // }).unwrap();
    return await sendRpcCommand({
      assetId: assetId,
      deviceId: deviceId,
      method: 'set_beep_status',
      value: true,
      params: true,
      persistent: false,
      timeout: 500,
      useStrictDataTypes: false,
    }).unwrap();
  };
  useEffect(() => {
    setSelected(SelectedMenu.List);
  }, [roomName]);
  useEffect(() => {
    setSelected(SelectedMenu.DeviceSettings);
  }, [deviceId]);
  useEffect(() => {
    setSelected(SelectedMenu.List);
  }, [assetId]);
  return (
    <Flex flexDirection={'column'} bgGradient="linear(to-t, #5EBDFD, #567CF8)">
      <Flex>
        <Image
          src="/images/Elehome.svg"
          h={'6.38vh'}
          w={'58.35vw'}
          fit={'contain'}
          mt={'12.46vh'}
        />
      </Flex>
      <Flex
        scrollBehavior={'initial'}
        style={{
          paddingRight: '1.3vw',
          paddingLeft: '1.3vw',
          marginTop: '5.9vh',
          flexDirection: 'column',
        }}
      >
        {selected === SelectedMenu.List ? (
          <ListOfButtons setSelected={setSelected} />
        ) : null}

        {selected === SelectedMenu.Edit ? (
          <ListOfEditButtons setSelected={setSelected} />
        ) : null}

        {selected === SelectedMenu.Room ? (
          <ListOfRoomButtons setSelected={setSelected} />
        ) : null}

        {selected === SelectedMenu.Home ? (
          <ListOfHomeButtons setSelected={setSelected} />
        ) : null}
        {selected === SelectedMenu.RoomEdit ? (
          <ListOfRomeEditButtons setSelected={setSelected} />
        ) : null}
        {selected === SelectedMenu.AssetSettings ? (
          <ListOfAssetSettingsButtons setSelected={setSelected} />
        ) : null}
        {selected === SelectedMenu.DeviceSettings ? (
          <ListOfDeviceSettingsButtons setSelected={setSelected} />
        ) : null}
      </Flex>
    </Flex>
  );
};

const ListOfButtons = ({
  setSelected,
}: {
  setSelected: (selectedMenu: SelectedMenu) => void;
}) => {
  interface Device {
    id: string;
    name: string;
    label: string;
    type: string;
    managerId: string;
    assetId: string;
    assetLabel: string;
    zone: string;
    deviceData: {
      configuration: {
        type: string;
      };
      transportConfiguration: {
        type: string;
      };
    };
    deviceProfileId: string;
  }
  interface Asset {
    id: string;
    name: string;
    type: string;
    label: string;
    managerId: string;
    ownerId: string;
  }
  const assetId = useReduxSelector((store) => store.asset.assetId);
  const roomName = useReduxSelector((store) => store.room.roomName);
  let devicesQuery = useGetAllDevicesQuery({ assetId: assetId }, {});
  const assetsQuery = useGetAssetsQuery(undefined, {});
  const [editDevice] = useEditDeviceMutation();
  const [expand, setExpand] = useState<boolean>(true);

  // const editZone = async (LName: string) => {
  //   await editDevice({
  //     assetId: assetId;
  //     deviceId: string;
  //     deviceIdentifier: string;
  //     label: string;
  //     type: string;
  //     zone: string;
  //     deviceProfileId: string;
  //   }
  //   );
  // };
  const getAssetsZonesNumber = (list: Device[] = []) => {
    // const result = new Map<string, Device[]>();
    let zoneNumber = [];
    list.forEach((device) => {
      if (zoneNumber.includes(device.zone)) {
      } else zoneNumber.push(device.zone);
    });
    return zoneNumber.length;
  };
  const getAssetDeviceNumber = (list: Device[] = []) => {
    // const result = new Map<string, Device[]>();
    let deviceNumber = 0;
    list.forEach((device) => {
      deviceNumber++;
    });
    return deviceNumber;
  };
  const getZonesDevicesNumber = (list: Device[] = []) => {
    // const result = new Map<string, Device[]>();
    let deviceNumber = 0;
    list.forEach((device) => {
      var itemZone = device.zone;
      if (itemZone === roomName) {
        deviceNumber++;
      }
    });
    return deviceNumber;
  };
  const getZonesAssetName = (list: Asset[] = []) => {
    let assetLabel = '123';
    list.forEach((asset) => {
      if (asset.id === assetId) {
        assetLabel = asset.label;
      }
    });
    return assetLabel;
  };
  const deviceId = useReduxSelector((store) => store.device.DeviceId);

  if (roomName !== 'assetsallroomsdevices')
    return (
      <>
        {/* <MenuButton
          editable={false}ن
          imageSrc="/images/EditProfile.svg"
          statisticsType={'string'}
          expandedEditable={'none'}
          statisticsString={deviceId}
        /> */}
        <MenuButton
          title={roomName != null ? roomName : 'تعیین نشده'}
          imageSrc="/images/EditProfile.svg"
          editable={true}
          statisticsNumber={0}
          statisticsType={'none'}
          expandedEditable={'none'} // updateName={() => setNameState(nameState)}
          // submitEdit={(input) => {
          //   setLastNameState(input);
          //   editLastNameSubmit(input);
          // }}
        />

        <MenuButton
          editable={false}
          title={'تعداد دستگاه'}
          imageSrc="/images/Notification.svg"
          onClick={() => {
            // setSelected(SelectedMenu.Room);
          }}
          statisticsNumber={getZonesDevicesNumber(devicesQuery.data)}
          statisticsType={'number'}
          expandedEditable={'none'}
        />
        <MenuButton
          editable={false}
          title="خانه"
          imageSrc="/images/Notification.svg"
          onClick={() => {
            // setSelected(SelectedMenu.Home);
            setExpand(!expand);
          }}
          statisticsString={getZonesAssetName(assetsQuery.data)}
          statisticsType={'string'}
          expandedEditable={expand ? 'none' : 'flex'}
          Array={assetsQuery.data}
        />
      </>
    );
  else if (roomName === 'assetsallroomsdevices')
    return (
      <>
        <MenuButton
          editable={true}
          title={getZonesAssetName(assetsQuery.data)}
          imageSrc="/images/Notification.svg"
          onClick={() => {
            // setSelected(SelectedMenu.Home);
            // setExpand(!expand);
          }}
          statisticsString={getZonesAssetName(assetsQuery.data)}
          statisticsType={'none'}
          expandedEditable={'none'}
        />
        <MenuButton
          editable={false}
          title={'تعداد اتاق ها'}
          imageSrc="/images/Notification.svg"
          onClick={() => {
            // setSelected(SelectedMenu.Room);
          }}
          statisticsNumber={getAssetsZonesNumber(devicesQuery.data)}
          statisticsType={'number'}
          expandedEditable={'none'}
        />

        <MenuButton
          editable={false}
          title={'تعداد دستگاه ها'}
          imageSrc="/images/Notification.svg"
          onClick={() => {
            // setSelected(SelectedMenu.Home);
            // setExpand(!expand);
          }}
          statisticsNumber={getAssetDeviceNumber(devicesQuery.data)}
          statisticsType={'number'}
          expandedEditable={'none'}
        />

        <MenuButton
          editable={false}
          title="مدیریت اشتراک ها"
          imageSrc="/images/Notification.svg"
          onClick={() => {
            setSelected(SelectedMenu.Home);
          }}
          statisticsNumber={0}
          statisticsType={'none'}
          expandedEditable={'none'}
        />
        <MenuButton
          editable={false}
          title="سایر تنظیمات"
          imageSrc="/images/Notification.svg"
          onClick={() => {
            setSelected(SelectedMenu.AssetSettings);
          }}
          statisticsNumber={0}
          statisticsType={'none'}
          expandedEditable={'flex'}
        />
        <MenuButton
          editable={true}
          title="ویرایش پروفایل"
          imageSrc="/images/EditProfile.svg"
          onClick={() => {
            setSelected(SelectedMenu.Edit);
          }}
          statisticsNumber={0}
          statisticsType={'none'}
          expandedEditable={'none'}
        />
      </>
    );
};

const ListOfEditButtons = ({
  setSelected,
}: {
  setSelected: (selectedMenu: SelectedMenu) => void;
}) => {
  return (
    <>
      <EditUserProfileScrollMenu />
    </>
  );
};

const ListOfRoomButtons = ({
  setSelected,
}: {
  setSelected: (selectedMenu: SelectedMenu) => void;
}) => {
  return (
    <>
      <MenuButton
        editable={true}
        title="نام اتاق"
        imageSrc="/images/EditProfile.svg"
        statisticsNumber={0}
        statisticsType={'none'}
        expandedEditable={'none'}
      />
      <div style={{ alignSelf: 'center', paddingTop: 10 }}>
        <Image
          src={'/back.svg'}
          width="52.756"
          height="53.732"
          viewBox="0 0 52.756 53.732"
          cursor={'pointer'}
          onClick={() => {
            setSelected(SelectedMenu.List);
          }}
        />
      </div>
    </>
  );
};

const ListOfHomeButtons = ({
  setSelected,
}: {
  setSelected: (selectedMenu: SelectedMenu) => void;
}) => {
  return (
    <>
      <MenuButton
        editable={true}
        title="نام خانه"
        imageSrc="/images/EditProfile.svg"
        statisticsNumber={0}
        statisticsType={'none'}
        expandedEditable={'none'}
      />
      <div style={{ alignSelf: 'center', paddingTop: 10 }}>
        <Image
          src={'/back.svg'}
          width="52.756"
          height="53.732"
          viewBox="0 0 52.756 53.732"
          cursor={'pointer'}
          onClick={() => {
            setSelected(SelectedMenu.List);
          }}
        />
      </div>
    </>
  );
};

const ListOfRomeEditButtons = ({
  setSelected,
}: {
  setSelected: (selectedMenu: SelectedMenu) => void;
}) => {
  return (
    <>
      <MenuButton
        editable={true}
        title=" 2نام خانه"
        imageSrc="/images/EditProfile.svg"
        statisticsType={'string'}
        expandedEditable={'flex'}
      />
      <div style={{ alignSelf: 'center', paddingTop: 10 }}>
        <Image
          src={'/back.svg'}
          width="52.756"
          height="53.732"
          viewBox="0 0 52.756 53.732"
          cursor={'pointer'}
          onClick={() => {
            setSelected(SelectedMenu.List);
          }}
        />
      </div>
    </>
  );
};
const ListOfAssetSettingsButtons = ({
  setSelected,
}: {
  setSelected: (selectedMenu: SelectedMenu) => void;
}) => {
  return (
    <>
      <MenuButton
        editable={true}
        title="حذف خانه"
        imageSrc="/images/EditProfile.svg"
        statisticsType={'string'}
        expandedEditable={'flex'}
      />
      <div style={{ alignSelf: 'center', paddingTop: 10 }}>
        <Image
          src={'/back.svg'}
          width="52.756"
          height="53.732"
          viewBox="0 0 52.756 53.732"
          cursor={'pointer'}
          onClick={() => {
            setSelected(SelectedMenu.List);
          }}
        />
      </div>
    </>
  );
};
const ListOfDeviceSettingsButtons = ({
  setSelected,
}: {
  setSelected: (selectedMenu: SelectedMenu) => void;
}) => {
  const assetId = useReduxSelector((store) => store.asset.assetId);
  const deviceId = useReduxSelector((store) => store.device.DeviceId);
  const deviceName = useReduxSelector((store) => store.device.DeviceName);
  const deviceZone = useReduxSelector((store) => store.device.DeviceZone);
  const roomName = useReduxSelector((store) => store.room.roomName);
  // const deviceZone = useReduxSelector((store) => store.device.);
  const [editDevice] = useEditDeviceMutation();
  const [deleteDevice] = useDeleteDeviceMutation();
  const [sendRpcCommand] = useSendRpcCommandMutation();

  let deviceInfo = useGetDeviceQuery({ assetId: assetId, id: deviceId }, {});
  let devicesQuery = useGetAllDevicesQuery({ assetId: assetId }, {});
  const [expand, setExpand] = useState<boolean>(true);

  const dispatch = useReduxDispatch();
  const router = useRouter();

  const editDeviceName = async (name: string) => {
    // console.log('edit device name');
    await editDevice({
      assetId: deviceInfo.data.assetId,
      deviceId: deviceInfo.data.id,
      deviceIdentifier: deviceInfo.data.name,
      label: name,
      type: deviceInfo.data.type,
      zone: deviceInfo.data.zone,
      deviceProfileId: deviceInfo.data.deviceProfileId,
    });
  };
  const editDeviceRoomName = async (roomName: string) => {
    // console.log('edit device name');

    await editDevice({
      assetId: deviceInfo.data.assetId,
      deviceId: deviceInfo.data.id,
      deviceIdentifier: deviceInfo.data.name,
      label: deviceInfo.data.label,
      type: deviceInfo.data.type,
      zone: roomName,
      deviceProfileId: deviceInfo.data.deviceProfileId,
    });
  };
  interface Device {
    id: string;
    name: string;
    label: string;
    type: string;
    managerId: string;
    assetId: string;
    assetLabel: string;
    zone: string;
    deviceData: {
      configuration: {
        type: string;
      };
      transportConfiguration: {
        type: string;
      };
    };
    deviceProfileId: string;
  }
  const getAssetsZones = (list: Device[] = []) => {
    let zoneNumber = [];
    list.forEach((device) => {
      if (zoneNumber.includes(device.zone)) {
      } else zoneNumber.push(device.zone);
    });
    return zoneNumber;
  };
  const deleteCurrentDevice = async () => {
    await deleteDevice({
      deviceId: deviceInfo.data.id,
    });
  };

  //ws setup
  const getDeviceToken = useGetDeviceTokenQuery(
    {
      assetId: assetId,
      deviceId: deviceId,
    },
    {}
  );
  const getDeviceTelemetries = useGetDeviceTelemetriesQuery(
    {
      entityType: 'DEVICE',
      entityId: deviceId,
      keys: '',
      useStrictDataTypes: false,
    },
    {}
  );
  const [socketUrl, setSocketUrl] = useState('wss://echo.websocket.org');

  const { sendMessage, lastMessage, readyState } = useWebSocket(socketUrl);
  const [signalStrength, setSignalStrength] = useState<string>();

  useEffect(() => {
    if (lastMessage !== null) {
      convertMap(map, JSON.parse(lastMessage.data).data);
      setSignalStrength(map.get('signal_strength_WiFi'));
    }
  }, [lastMessage]);

  useEffect(() => {
    if (readyState == ReadyState.OPEN) {
      var object = {
        tsSubCmds: [
          {
            entityType: 'DEVICE',
            entityId: deviceId,
            scope: 'LATEST_TELEMETRY',
            cmdId: 10,
          },
        ],
        historyCmds: [],
        attrSubCmds: [],
      };
      var data = JSON.stringify(object);
      sendMessage(data);
    }
  }, [readyState]);

  const handleClickChangeSocketUrl = useCallback(async () => {
    var token = await getDeviceToken.data?.token;
    var url = 'ws://95.80.182.57:8080/api/ws/plugins/telemetry?token=' + token;
    setSocketUrl(url);
  }, []);

  useEffect(() => {
    handleClickChangeSocketUrl();
  }, []);

  const connectionStatus = {
    [ReadyState.CONNECTING]: 'Connecting',
    [ReadyState.OPEN]: 'Open',
    [ReadyState.CLOSING]: 'Closing',
    [ReadyState.CLOSED]: 'Closed',
    [ReadyState.UNINSTANTIATED]: 'Uninstantiated',
  }[readyState];
  const [map, setMap] = useState<Map<string, string>>(
    new Map<string, string>()
  );
  const convertMap = (
    map: Map<string, string>,
    obj: { [key: string]: string }
  ) => {
    if (obj != null) {
      for (const [key, value] of Object.entries(obj)) {
        const mainValue = Array.from(value as string)[0][1];
        map.set(key, mainValue);
      }
      setMap(map);
    }
  };

  return (
    <>
      <MenuButton
        editable={true}
        title={deviceName}
        imageSrc="/images/Notification.svg"
        // statisticsString={getZonesAssetName(assetsQuery.data)}
        statisticsType={'none'}
        expandedEditable={'none'}
        submitEdit={(input) => {
          editDeviceName(input);
          dispatch(setDeviceName(input));
          // router.reload();
        }}
      />

      <MenuButton
        editable={true}
        title={deviceZone}
        imageSrc="/images/Notification.svg"
        // statisticsString={getZonesAssetName(assetsQuery.data)}
        statisticsType={'none'}
        expandedEditable={'none'}
        submitEdit={(input) => {
          editDeviceRoomName(input);
          // router.reload();
        }}
      />

      <MenuButton
        editable={false}
        title={'حذف دستگاه'}
        imageSrc="/images/Notification.svg"
        // statisticsString={getZonesAssetName(assetsQuery.data)}
        statisticsType={'none'}
        expandedEditable={'none'}
        onClick={() => {
          deleteCurrentDevice();
          // router.reload();
        }}
      />
      <MenuButton
        editable={false}
        title={'قدرت سیگنال'}
        imageSrc="/images/Notification.svg"
        // statisticsString={getZonesAssetName(assetsQuery.data)}
        statisticsType={'string'}
        expandedEditable={'none'}
        statisticsString={signalStrength}
      />
    </>
  );
};
