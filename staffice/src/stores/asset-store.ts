import { PayloadAction, createSlice } from '@reduxjs/toolkit';

export interface CounterState {
  assetId: string;
  roomName: string;
}

const initialState: CounterState = {
  assetId: '',
  roomName: 'sss',
};

export const assetSlice = createSlice({
  name: 'asset',
  initialState,
  reducers: {
    setAsset: (state, action: PayloadAction<string>) => {
      state.assetId = action.payload;
    },
  },
});
export const roomNameSlice = createSlice({
  name: 'roomName',
  initialState,
  reducers: {
    setRoomName: (state, action: PayloadAction<string>) => {
      state.roomName = action.payload;
    },
  },
});

export const { setAsset } = assetSlice.actions;
export const { setRoomName } = roomNameSlice.actions;
