import { PayloadAction, createSlice } from '@reduxjs/toolkit';

export interface SideMenuState {
  sideMenuStatus: 'expand' | 'shrink';
  sideMenuContent: 'asset' | 'room' | 'device';
}

const initialState: SideMenuState = {
  sideMenuStatus: 'shrink',
  sideMenuContent: 'asset',
};

export const sideMenuSlice = createSlice({
  name: 'sideMenu',
  initialState,
  reducers: {
    setSideMenuStatus: (state, action: PayloadAction<'expand' | 'shrink'>) => {
      state.sideMenuStatus = action.payload;
    },
    setSideMenuContent: (
      state,
      action: PayloadAction<'asset' | 'room' | 'device'>
    ) => {
      state.sideMenuContent = action.payload;
    },
  },
});

export const { setSideMenuStatus, setSideMenuContent } = sideMenuSlice.actions;
