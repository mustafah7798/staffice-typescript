import React, { Suspense, useEffect } from 'react';

import { ReactQueryDevtools } from 'react-query/devtools';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';

import { PageLogout } from '@/app/auth-archive/PageLogout';
import { LoginScreen } from '@/app/auth/LoginScreen';
import { Layout, Loader } from '@/app/layout';
import {
  AdminRouteGuard,
  AuthenticatedRouteGuard,
  PublicOnlyRouteGuard,
} from '@/app/router/guards';
import { Error404, ErrorBoundary } from '@/errors';

import { DeviceScrollMenu } from '../components/scroll-view/DeviceScrollMenu';
import { EditUserProfileScrollMenu } from '../components/scroll-view/EditUserProfileScrollMenu';
import { HomeScrollMenu } from '../components/scroll-view/HomeScrollMenu';
import { MapScrollMenu } from '../components/scroll-view/MapScrollMenu';
import { SettingScrollMenu } from '../components/scroll-view/SettingScrollMenu';
import { StatisticScrollMenu } from '../components/scroll-view/StatisticScrollMenu';
import { HomeScreen } from './home/HomeScreen';

const AdminRoutes = React.lazy(() => import('@/app/admin/AdminRoutes'));
const AccountRoutes = React.lazy(() => import('@/app/account/AccountRoutes'));
const DashboardRoutes = React.lazy(
  () => import('@/app/dashboard/DashboardRoutes')
);
let socket;

export const App = () => {
  // useEffect(() => {
  //   // const socketInitializer = async () => {
  //   //   await fetch('/service/api/socket');
  //   //   socket = io();

  //   //   socket.on('connect', () => {
  //   //     console.log('connectedDDDDDDDDD');
  //   //   });
  //   // };
  //   // socketInitializer();
  //   const fetchData = async () => {
  //     await fetch('/api/socket/socket');
  //     await console.log(await fetch('/api/socket/socket'));

  //     socket = io();
  //     socket.on('connect', () => {
  //       console.log('connectedDDDDDDDDD');
  //     });
  //   };

  //   fetchData()
  //     // make sure to catch any error
  //     .catch(console.error);
  // }, []);
  return (
    <ErrorBoundary>
      <BrowserRouter basename="/app">
        <Layout>
          <Suspense fallback={<Loader />}>
            <Routes>
              <Route path="/" element={<Navigate to="/homePage" replace />} />

              <Route
                path="homePage"
                element={
                  <PublicOnlyRouteGuard>
                    <HomeScreen />
                  </PublicOnlyRouteGuard>
                }
              />
              <Route
                path="login"
                element={
                  <PublicOnlyRouteGuard>
                    <LoginScreen />
                  </PublicOnlyRouteGuard>
                }
              />

              <Route
                path="logout"
                element={
                  <ErrorBoundary>
                    <PageLogout />
                  </ErrorBoundary>
                }
              />

              <Route
                path="editUserProfile"
                element={
                  <PublicOnlyRouteGuard>
                    <EditUserProfileScrollMenu />
                  </PublicOnlyRouteGuard>
                }
              />

              {/* <Route
                path="homeScrollMenu"
                element={
                  <PublicOnlyRouteGuard>
                    <HomeScrollMenu />
                  </PublicOnlyRouteGuard>
                }
              />

              <Route
                path="mapScrollMenu"
                element={
                  <PublicOnlyRouteGuard>
                    <MapScrollMenu />
                  </PublicOnlyRouteGuard>
                }
              />
              <Route
                path="deviceScrollMenu"
                element={
                  <PublicOnlyRouteGuard>
                    <DeviceScrollMenu />
                  </PublicOnlyRouteGuard>
                }
              />
              <Route
                path="editUserProfileScrollMenu"
                element={
                  <PublicOnlyRouteGuard>
                    <EditUserProfileScrollMenu />
                  </PublicOnlyRouteGuard>
                }
              />
              <Route
                path="settingScrollMenu"
                element={
                  <PublicOnlyRouteGuard>
                    <SettingScrollMenu />
                  </PublicOnlyRouteGuard>
                }
              />
              <Route
                path="statisticScrollMenu"
                element={
                  <PublicOnlyRouteGuard>
                    <StatisticScrollMenu />
                  </PublicOnlyRouteGuard>
                }
              /> */}

              <Route
                path="account/*"
                element={
                  <ErrorBoundary>
                    <AccountRoutes />
                  </ErrorBoundary>
                }
              />

              <Route
                path="dashboard/*"
                element={
                  <AuthenticatedRouteGuard>
                    <DashboardRoutes />
                  </AuthenticatedRouteGuard>
                }
              />

              <Route
                path="admin/*"
                element={
                  <AdminRouteGuard>
                    <AdminRoutes />
                  </AdminRouteGuard>
                }
              />

              <Route path="*" element={<Error404 />} />
            </Routes>
          </Suspense>
        </Layout>
      </BrowserRouter>
      <ReactQueryDevtools />
    </ErrorBoundary>
  );
};
